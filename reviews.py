import re
import pandas as pd
from sklearn.model_selection import train_test_split
from nltk.sentiment.util import mark_negation

class Reviews:
    def __init__(self, args, **options):
        self.args = args
        self.filename = args.filename
        nrows = self.args.max_count
        if options.get('test', False):
            self.filename = self.filename.replace('train', 'test')
            nrows = 999999
        self.reviews = pd.read_json(self.filename,
                                    lines=True,
                                    nrows=nrows)
        print("reviews read: %d" % self.reviews.shape[0])

    def mark_negation_str(txt):
        #txt = 'The book was not good to read'
        #txt = "The book wasn't good to read. It stank!"
        txt = re.sub(r'\.', ' . ',txt)
        ntxt = mark_negation(txt.split())
        txt = ' '.join(ntxt)
        txt = re.sub(r'\.\.+', ' _EL ',txt)
        txt = re.sub(r'\.', ' _FS ',txt)
        txt = re.sub(r',+', ' _CM ',txt)
        txt = re.sub(r'\!+', ' _EX ',txt)
        txt = re.sub(r'\?+', ' _QN ',txt)
        txt = re.sub(r'\"+', ' _QU ',txt)
        txt = re.sub(r'\(', ' _PL ',txt)
        txt = re.sub(r'\)', ' _PR ',txt)
        return(txt)

    def mark_negation(self):
        self.reviews['reviewTextNeg'] = \
            self.reviews.reviewText.apply(lambda x: \
                                          Reviews.mark_negation_str(x))

    cheats = {
        5: 'xgoodxgood',
        4: 'goodgood',
        3: 'neutralneutral',
        2: 'badbad',
        1: 'xbadxbad',
        }

    def extract_prefix(self):
        n = self.args.prefix
        if n <= 0:
            return
        def prefix(s, n):
            ary = s.split(None, n)
            ary.pop()
            return ' '.join(ary)
        self.reviews.reviewTextNeg = \
                self.reviews.reviewTextNeg.apply(lambda x:
                                                 prefix(x, n))

    def add_extra(self):
        if not self.args.extra:
            return

        self.reviews.reviewText = \
            list(map(lambda pair: \
                     "{}. {}".format(self.cheats[pair[1]], pair[0][0:1000]),
                     zip(self.reviews.reviewText,
                         self.reviews.overall)))


    def filter_unused(self, src, labels):
        filtered = list(filter(lambda pair: 0 <= pair[1],
                            zip(src, labels)))
        return list(map(lambda pair: pair[0], filtered))

    def make_data(self, xform):
        self.add_extra()
        self.mark_negation()
        self.extract_prefix()

        col = 'reviewTextNeg'
        col = 'summary'
        if xform == None:
            train = self.reviews[col]
        else:
            train = xform.fit_transform(self.reviews[col])
        self.make_labels()

        labels = self.reviews['label']
        weights = self.make_weights(self.args.weights)

        train = self.filter_unused(train, labels)
        weights = self.filter_unused(weights, labels)
        labels = self.filter_unused(labels, labels)
        print("labels: {} records".format(len(labels)))
        print("train: {} records".format(len(train)))
        print("weights: {} records".format(len(weights)))
        self.split_data = train_test_split(train, labels, weights)

    def balanced(self, total, counts, label):
        return total / counts[label]

    def quadratic(self, total, counts, label):
        x = (total / counts[label]) ** 2
        return x

    def cubic(self, total, counts, label):
        x = (total / counts[label]) ** 3
        return x

    def quartic(self, total, counts, label):
        x = (total / counts[label]) ** 4 / 100.0
        return x

    def custom(self, total, counts, label):
        return [200.0, 400.0, 0.0001][label]

    def make_weights(self, method):
        labels = self.reviews['label']
        counts = labels.value_counts()
        total = float(len(labels))
        if method == 'balanced':
            return labels.\
                apply(lambda label: self.balanced(total,
                                                  counts,
                                                  label))
        if method == 'uniform':
            return labels.apply(lambda label : 1)
        if method == 'quadratic':
            return labels.\
                apply(lambda label: self.quadratic(total,
                                                   counts,
                                                   label))
        if method == 'cubic':
            return labels.\
                apply(lambda label: self.cubic(total,
                                               counts,
                                               label))
        if method == 'quartic':
            return labels.\
                apply(lambda label: self.quartic(total,
                                                 counts,
                                                 label))
        if method == 'custom':
            return labels.\
                apply(lambda label: self.custom(total,
                                                counts,
                                                label))


    def training_data(self, xform):
        self.make_data(xform)
        return (self.split_data[0], self.split_data[2], self.split_data[4])

    def minimal_accuracy(self, labels):
        if type(labels) == list:
            df = pd.DataFrame()
            df['label'] = labels
            labels = df.label
        counts = labels.value_counts()
        mode = -1
        mc = 0
        for label, cnt in counts.items():
            print(label, " : ", cnt)
            if 0 <= label and mc < cnt:
                mc = cnt
                mode = label
        return counts[mode] / (counts.sum() - counts.get(-1, 0))

    def testing_data(self):
        print("overall minimal accuracy: %0.4f" % \
              self.minimal_accuracy(self.reviews.label))
        print("testing minimal accuracy : %0.4f" % \
              self.minimal_accuracy(self.split_data[3]))
        return (self.split_data[1], self.split_data[3])

    def to_label(value, ary):
        return ary[int(value)]

    labels_012 = [None, 0, 0, 1, 2, 2]
    labels_02 = [None, 0, 0, 0, 2, 2]
    labels_01 = [None, 0, 0, 1, -1, -1]

    def make_labels(self):
        labels = None
        if self.args.labels == '012':
            labels = self.reviews['overall'].\
                apply(lambda x: Reviews.to_label(x, self.labels_012))
        if self.args.labels == '02':
            labels = self.reviews['overall'].\
                apply(lambda x: Reviews.to_label(x, self.labels_02))
        if self.args.labels == '01':
            labels = self.reviews['overall'].\
                apply(lambda x: Reviews.to_label(x, self.labels_01))
        self.reviews['label'] = labels


    def make_meta(self):
        self.reviews['length10'] = \
            self.reviews['reviewText'].apply(lambda x: 10 * int(len(x) / 10))

    def meta_data(self):
        self.make_meta()
        df = pd.DataFrame(data=self.reviews,
                          columns=['length10', 'label'])
        return df
