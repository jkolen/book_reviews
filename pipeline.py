#!/usr/bin/env python
from contextlib import redirect_stdout
import io
from pprint import pprint
import os
import sys
from time import time

import pandas as pd
import joblib
from sklearn.metrics \
    import make_scorer, precision_score, accuracy_score, recall_score
from sklearn.model_selection import GridSearchCV

from helpers import *
from reviews import Reviews
from pipeline_builder import PipelineBuilder, clean_tmp

args = cl_args()
pprint(args)

def sub_scorer(score, index):
    def calculate(y_true, y_pred, **kwargs):
        s = score(y_true, y_pred, average=None, zero_division=0)
        if index < len(s):
            return s[index]
        else:
            return -1
        
    return lambda y_true, y_pred, **kwargs : calculate(y_true, y_pred, **kwargs)

scoring_info = {
    '012': ['neg', 'neutral', 'pos'],
    '02': ['neg/neutral', 'pos'],
    '01': ['neg', 'neutral']
}

if __name__ == "__main__":
    pb = PipelineBuilder(*args.components)

    pipeline = pb.pipeline()
    parameters = pb.parameters(args.parameters)
    pprint(parameters)

    reviews = Reviews(args)

    x_train, y_train, weights_train = reviews.training_data(None)

    # find the best parameters for both the feature extraction and the
    # classifier
    n_jobs = args.n_jobs
    pre_dispatch = args.pre_dispatch
    if args.max_count < 50000:
        if n_jobs == -1:
            n_jobs = -1
        if pre_dispatch == -1:
            pre_dispatch = 4
        if args.max_count < 20000:
            if pre_dispatch == -1:
                pre_dispatch = 8

    grid_search = GridSearchCV(pipeline,
                               parameters,
                               pre_dispatch=pre_dispatch,
                               n_jobs=n_jobs,
                               verbose=1)

    print("Performing grid search...")
    print("pipeline:", [name for name, _ in pipeline.steps])
    print("parameters:")
    pprint(parameters)
    t0 = time()
    kwargs = {"{}__sample_weight".format(args.components[-1]): weights_train}
    grid_search.fit(x_train, y_train, **kwargs)
    elapsed = time() - t0

    report = 'nothing'
    
    with io.StringIO() as buf, redirect_stdout(buf):
        print("=" * 60)
        print(' '.join(sys.argv))
        print("done in %0.3fs" % (elapsed,))
        pprint(parameters)
        print("Best score: %0.3f" % grid_search.best_score_)
        print("Best parameters set:")
        best_parameters = grid_search.best_estimator_.get_params()
        for param_name in sorted(parameters.keys()):
            print("\t%s: %r" % (param_name, best_parameters[param_name]))
            y_est = grid_search.predict(x_train)

        header('Training Data Performance')
        scoring_dict = {
            'accuracy': accuracy_score,
        }
        for idx, label in enumerate(scoring_info[args.labels]):
            scoring_dict[label + '_precision'] = \
                sub_scorer(precision_score, idx)
            scoring_dict[label + '_recall'] = \
                sub_scorer(recall_score, idx)

        for label, scorer in scoring_dict.items():
            print("%-25s: %0.4f" % (label, scorer(y_train, y_est)))

        header('Test Data Performance')

        x_test, y_test = reviews.testing_data()
        y_test_est = grid_search.predict(x_test)
        for label, scorer in scoring_dict.items():
            print("%-25s: %0.4f" % (label, scorer(y_test, y_test_est)))

        report = buf.getvalue()

    print(report)
    print()
    x = input("save to notebook.txt [Yn]?")
    if x == 'y' or x == '':
        print("writing")
        out = open('notebook.txt', 'a')
        out.write(report)
        out.close()
    clean_tmp()
    joblib.dump(grid_search.best_estimator_, 'model.pkl', compress=1)

    
