import importlib
import os
import yaml

from sklearn.pipeline import Pipeline


TMP_DIR = 'tmp/pipeline'
def clean_tmp():
    os.system('rm -rf {}/*'.format(TMP_DIR))

clean_tmp()

class PipelineBuilder:
    def __init__(self, *phases):
        self.phases = list(phases)

    @staticmethod
    def make_param(d, key, value):
        def cvt(value):
            if type(value) == str:
                return eval(value)
            return value
        if (type(value) == list):
            d[key] = \
                list(map(cvt, value))
        else:
            d[key] = value

    @staticmethod
    def make_component(d, label, args):
        d[label] = args

    params = None
    @classmethod
    def make_params(cls, filename = 'parameters.yaml'):
        if cls.params != None:
            return
        f = open(filename)
        src = yaml.load(f, Loader=yaml.FullLoader)
        dest = {}
        for k, v in src.items():
            dest[k] = {}
            if k == 'components':
                for component, arg_list in v.items():
                    cls.make_component(dest[k], component, arg_list)
            else:
                for group, component in v.items():
                    dest[k][group] = {}
                    for param, arg_list in component.items():
                        cls.make_param(dest[k][group], param, arg_list)
        cls.components = dest.pop('components', None)
        if cls.components == None:
            raise KeyError('missing component from {}'.format(filename))
        cls.params = dest
        f.close()

    @classmethod
    def get(cls, key):
        cls.make_params()
        return cls.params.get(key, None)
    
    def pipeline(self):
        ppln = Pipeline(list(map(lambda p : self.instantiate(p), self.phases)),
                        memory=TMP_DIR,
                        verbose=True)
        return ppln

    def instantiate(self, phase):
        component = self.components[phase]
        class_name = None
        module_name = None
        kwargs = {}
        for k, v in component.items():
            if k == 'class':
                class_name = v
            else:
                if k == 'module':
                    module_name = v
                else:
                    kwargs[k] = eval(v)
        klass = self.instantiate_class(module_name, class_name)
        return (phase, klass(**kwargs))

    def instantiate_class(self, module_name, class_name):
        module = importlib.import_module(module_name)
        return getattr(module, class_name)

    def old_instantiate_class(self, module_name, class_name):
        module = None
        for m in module_name.split('.'):
            if module == None:
                module = __import__(m)
            else:
                module = getattr(module, m)
        return getattr(module, class_name)

    def component_class(self, name):
        component = self.components[name]
        return self.instantiate_class(component['module'],
                                      component['class'])
        
    def parameters(self, context):
        rv = {}
        for phase in self.phases:
            rv.update(self.get_params(phase, context))
        return rv

    def get_params(self, phase, context):
        px = self.params[context][phase]
        rv = {}
        for k, v in px.items():
            rv['{}__{}'.format(phase, k)] = v
        return rv

PipelineBuilder.make_params()
