# Book Reviews

Demo for specifying SKLearn pipelines from the command line to process
data set for rating prediction.

For example:

```
./pipeline.py -n 1000 -p quick -w cubic reviews_train.json hvect tfidf clf
```

This command will construct a pipeline consisting of a
HashingVectorizer, a TfidfTransformer, and SGDClassifier. It will run on the first 1000 JSON objects found in medium_reviews_train.json. It will also weight
the training examples by the inverse cube of the number of items in the class (N/n^3).

Parameters for the stages are found in parameters.yaml. The program will perform grid search across the combinations of values described in this configuration file. All parameters should be arrays (delimited by square brackets). The any values read in as strings will be eval'ed in order to get at class names. Hence
strings must be delimite as such: '"string value"'. When it's eval'ed, it comes out as a string.

# Output
At the end of the run, it will produce a report such as the one below. It will provide information about the training as well as testing statistics. The data is far from balanced, so you can get high accuracy (~83%) with a constant classifier.
```
done in 6.352s
{'clf__alpha': [1e-06],
 'clf__max_iter': [20],
 'clf__penalty': ['elasticnet'],
 'hvect__alternate_sign': [False],
 'hvect__lowercase': [True],
 'hvect__n_features': [262144],
 'hvect__ngram_range': [[1, 3]],
 'hvect__stop_words': [None],
 'hvect__token_pattern': ['[0-9_A-Za-z]+?[0-9_A-Za-z]'],
 'tfidf__norm': ['l2'],
 'tfidf__smooth_idf': [True],
 'tfidf__sublinear_tf': [False],
 'tfidf__use_idf': [True]}
Best score: 0.128
Best parameters set:
	clf__alpha: 1e-06
	clf__max_iter: 20
	clf__penalty: 'elasticnet'
	hvect__alternate_sign: False
	hvect__lowercase: True
	hvect__n_features: 262144
	hvect__ngram_range: [1, 3]
	hvect__stop_words: None
	hvect__token_pattern: '[0-9_A-Za-z]+?[0-9_A-Za-z]'
	tfidf__norm: 'l2'
	tfidf__smooth_idf: True
	tfidf__sublinear_tf: False
	tfidf__use_idf: True
***************** Training Data Performance ******************
accuracy                 : 0.2800
neg_precision            : 0.1060
neg_recall               : 1.0000
neutral_precision        : 0.4118
neutral_recall           : 1.0000
pos_precision            : 1.0000
pos_recall               : 0.1318
******************* Test Data Performance ********************
2  :  830
1  :  106
0  :  64
overall minimal accuracy: 0.8300
2  :  208
1  :  29
0  :  13
testing minimal accuracy : 0.8320
accuracy                 : 0.1440
neg_precision            : 0.0521
neg_recall               : 0.7692
neutral_precision        : 0.1795
neutral_recall           : 0.2414
pos_precision            : 1.0000
pos_recall               : 0.0913

```

# Usage

First, make sure the data files are unpacked. These are smaller versions of
a much larger data set.

```
gunzip medium_train.json.gz medium_test.json.gz
```

The program command line arguments are listed below:

```
usage: pipeline.py [-h] [-f FOLDS] [-n MAX_COUNT] [-s SKIP] [-p PARAMETERS]
                   [-w WEIGHTS] [-l LABELS] [--prefix N] [-j N_JOBS]
		   [-J PRE_DISPATCH] [-x]
                   FILE CMP [CMP ...]

positional arguments:
  FILE                  json file with objects to process
  CMP                   json file with objects to process

optional arguments:
  -h, --help            show this help message and exit
  -f FOLDS, --folds FOLDS
                        number of folds
  -n MAX_COUNT, --max_count MAX_COUNT
                        number of object read
  -s SKIP, --skip SKIP  number of objects to skip before reading
  -p PARAMETERS, --parameters PARAMETERS
                        parameter set
  -w WEIGHTS, --weights WEIGHTS
                        classifier to transform strings
  -l LABELS, --labels LABELS
                        classifier to transform strings
  --prefix N            take the first N words
  -j N_JOBS, --n_jobs N_JOBS
                        number of jobs to run
  -J PRE_DISPATCH, --pre_dispatch PRE_DISPATCH
                        number of jobs to preload
  -x, --extra           add extra to text
```
