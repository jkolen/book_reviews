from argparse import ArgumentParser
import os
import subprocess

import numpy

def cl_args():
    parser = ArgumentParser(description="naive bayes")
    parser.add_argument('-f', '--folds',
                        type=int,
                        default=4,
                        help='number of folds')
    parser.add_argument('-n', '--max_count',
                        type=int,
                        default=-1,
                        help='number of object read')
    parser.add_argument('-s', '--skip',
                        type=int,
                        default=0,
                        help='number of objects to skip before reading')
    parser.add_argument('-p', '--parameters',
                        type=str,
                        default='quick',
                        help='parameter set')
    parser.add_argument('-w', '--weights',
                        type=str,
                        default='balanced',
                        help='classifier to transform strings')
    parser.add_argument('-l', '--labels',
                        type=str,
                        default='012',
                        help='classifier to transform strings')
    parser.add_argument('--prefix',
                        type=int,
                        metavar='N',
                        default=-1,
                        help='take the first N words')
    parser.add_argument('-j','--n_jobs',
                        type=int,
                        default=-1,
                        help='number of jobs to run')
    parser.add_argument('-J', '--pre_dispatch',
                        type=int,
                        default=-1,
                        help='number of jobs to preload')
    parser.add_argument('-x', '--extra',
                        action='store_true',
                        help='add extra to text')
    parser.add_argument('filename',  metavar='FILE', type=str,
                        help='json file with objects to process')

    parser.add_argument('components',  metavar='CMP', type=str,
                        nargs='+',
                        help='json file with objects to process')

    args = parser.parse_args()

    if args.max_count == -1:
        out = subprocess.run(['wc', '-l', args.filename],
                             stdout=subprocess.PIPE)
        args.max_count = int(out.stdout.strip().split(b' ')[0])
    return args

def header(title, width=60):
    stars = width - len(title)
    left = int(stars / 2)
    print('{} {} {}'.format('*' * left, title, '*' * (stars - left)))

def summarize(results, folds):
    for fold in range(folds):
        print('-' * 60)
        print("Fold {}".format(fold + 1))
        for label, array in results.items():
            print("%-25s: %0.4f" % (label, array[fold]))
    print('=' * 60)
    for label, array in results.items():
        m = numpy.mean(array)
        sd = numpy.std(array)
        print("%-25s: %0.4f (%0.4f)" % (label, m, sd))
